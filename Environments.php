<html xmlns:align="http://www.w3.org/1999/xhtml">
<?php
require 'header.php';
echo head("Справочник сред", "market6.css", "");
?>
<body>
<p align="right"><img src="https://img.icons8.com/ios/40/4a90e2/boy.png">Vatkin</p>
<h1>Справочник сред</h1>
<br>
<div class = "space">
    <div>
        <?php
        echo require 'nav.php';
        ?>
    </div>
	<div>
		<table align: center style = "width: 1200px">
            <tbody>
            <tr>
                <th>Названия</th>
                <th>Описание</th>
                <th>Действия</th>
            </tr>
			<?php
				require "func.php";
				$sql = 'SELECT * FROM environments';
				$stm = return_array($sql);
				foreach ($stm as $data)	{
					echo "<tr>".PHP_EOL;
					echo "<td>".$data['Name']."</td>".PHP_EOL;
					echo "<td>".$data['Description']."</td>".PHP_EOL;
					echo "<td><form action = 'handler.php' method = 'POST'><button type = 'submit' style = 'background-color: red' value ='".$data['Name']."&DeleteEnv' name = 'button'>Удалить</button></form></td>".PHP_EOL;
					echo "</tr>".PHP_EOL;
				}
			?>
			</tbody>
		</table>
		<br>
		<?php
			echo "<form action = 'AddNewEnvironment.php'><button style = 'background-color: blue'>Добавить новую среду</button></form>";
		?>
	</div>
</div>
</body>
</html>