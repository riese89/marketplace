<?php
class formData
{
    private $orderName;
    private $author;
    private $environment;
    private $arApp0;
    private $arApp1;
    private $arDb0;
    private $arDb1;
    private $arBal0;
    private $arBal1;
    private $sum;
    private $sla;
    private $dataCr;
    private $dataMd;
    private $orderStatus;
    private $order_id;
    private $numOrder;
    private $nowDate;
    /**
     * @return mixed
     */
    public function getOrderName()
    {
        return $this->orderName;
    }

    /**
     * @param mixed $orderName
     */
    public function setOrderName($orderName)
    {
        $this->orderName = $orderName;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * @param mixed $environment
     */
    public function setEnvironment($environment)
    {
        $this->environment = $environment;
    }

    /**
     * @return mixed
     */
    /**
     * @return mixed
     */
    public function getArApp0()
    {
        return $this->arApp0;
    }

    /**
     * @param mixed $arApp0
     */
    public function setArApp0($arApp0)
    {
        $this->arApp0 = $arApp0;
    }

    /**
     * @return mixed
     */
    public function getArApp1()
    {
        return $this->arApp1;
    }

    /**
     * @param mixed $arApp1
     */
    public function setArApp1($arApp1)
    {
        $this->arApp1 = $arApp1;
    }

    /**
     * @return mixed
     */
    public function getArDb0()
    {
        return $this->arDb0;
    }

    /**
     * @param mixed $arDb0
     */
    public function setArDb0($arDb0)
    {
        $this->arDb0 = $arDb0;
    }

    /**
     * @return mixed
     */
    public function getArDb1()
    {
        return $this->arDb1;
    }

    /**
     * @param mixed $arDb1
     */
    public function setArDb1($arDb1)
    {
        $this->arDb1 = $arDb1;
    }

    /**
     * @return mixed
     */
    public function getArBal0()
    {
        return $this->arBal0;
    }

    /**
     * @param mixed $arBal0
     */
    public function setArBal0($arBal0)
    {
        $this->arBal0 = $arBal0;
    }

    /**
     * @return mixed
     */
    public function getArBal1()
    {
        return $this->arBal1;
    }

    /**
     * @param mixed $arBal1
     */
    public function setArBal1($arBal1)
    {
        $this->arBal1 = $arBal1;
    }
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * @param mixed $sum
     */
    public function setSum($sum)
    {
        $this->sum = $sum;
    }

    /**
     * @return mixed
     */
    public function getSla()
    {
        return $this->sla;
    }

    /**
     * @param mixed $sla
     */
    public function setSla($sla)
    {
        $this->sla = $sla;
    }

    /**
     * @return mixed
     */
    public function getDataCr()
    {
        return $this->dataCr;
    }

    /**
     * @param mixed $dataCr
     */
    public function setDataCr($dataCr)
    {
        $this->dataCr = $dataCr;
    }

    /**
     * @return mixed
     */
    public function getDataMd()
    {
        return $this->dataMd;
    }

    /**
     * @param mixed $dataMd
     */
    public function setDataMd($dataMd)
    {
        $this->dataMd = $dataMd;
    }

    /**
     * @return mixed
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    /**
     * @param mixed $orderStatus
     */
    public function setOrderStatus($orderStatus)
    {
        $this->orderStatus = $orderStatus;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * @param mixed $order_id
     */
    public function setOrderId($order_id)
    {
        $this->order_id = $order_id;
    }

    /**
     * @return mixed
     */
    public function getNumOrder()
    {
        return $this->numOrder;
    }

    /**
     * @param mixed $numOrder
     */
    public function setNumOrder($numOrder)
    {
        $this->numOrder = $numOrder;
    }

    /**
     * @return mixed
     */
    public function getNowDate()
    {
        return $this->nowDate;
    }

    /**
     * @param mixed $nowDate
     */
    public function setNowDate($nowDate)
    {
        $this->nowDate = $nowDate;
    }
}